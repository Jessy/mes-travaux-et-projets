<html>
<head>
	<link rel="stylesheet" href="style.css">
	<title>AffichageUsagersAges</title>
	<style>
	table, th, td {
 	border:1px solid black;
	}
	</style>
</head>
<body>
	<?php
		include('verifauthentification.php');
		verifConnexion();
		include('menuglobal.php');
	?>
	<h1>Répartition des usagers</h1>
		<table>
			<thead>
				<tr>
					<th>Tranche d'âge</th>
					<th>Nb Hommes</th>
					<th>Nb Femmes</th>
				</tr>
			</thead>
			<tbody>
			<?php
				//connexion à la base de données
				require('connexionBaseDonnees.php');
				$link = connexionBD();

				//requête de selection des usagers hommes de moins de 25 ans
				$requeteH = 'SELECT count(*) as total FROM usager WHERE DATEDIFF(NOW(), usager.Date_Naissance)/365 < 25 AND usager.Civilite="Mr"';
				$resqueryH = $link->prepare($requeteH);
				$resqueryH -> execute();
				$resH = $resqueryH->fetch();

				//requête de selection des usagers femmes de moins de 25 ans
				$requeteF = 'SELECT count(*) as total FROM usager WHERE DATEDIFF(NOW(), usager.Date_Naissance)/365 < 25 AND usager.Civilite="Md"';
				$resqueryF = $link->prepare($requeteF);
				$resqueryF -> execute();
				$resF = $resqueryF->fetch();

				echo "<tr><td> Moins de 25 ans </td><td>".$resH['total']."</td><td>".$resF['total']."</td></tr>";


				//requête de selection des usagers hommes entre 25 et 50 ans
				$requeteH = 'SELECT count(*) as total FROM usager WHERE DATEDIFF(NOW(), usager.Date_Naissance)/365 between 25 and 50 AND usager.Civilite="Mr"';
				$resqueryH = $link->prepare($requeteH);
				$resqueryH -> execute();
				$resH = $resqueryH->fetch();

				//requête de selection des usagers femmes entre 25 et 50 ans
				$requeteF = 'SELECT count(*) as total FROM usager WHERE DATEDIFF(NOW(), usager.Date_Naissance)/365 between 25 and 50 AND usager.Civilite="Md"';
				$resqueryF = $link->prepare($requeteF);
				$resqueryF -> execute();
				$resF = $resqueryF->fetch();

				echo "<tr><td> Entre 25 et 50 ans </td><td>".$resH['total']."</td><td>".$resF['total']."</td></tr>";


				//requête de selection des usagers hommes de plus de 50 ans
				$requeteH = 'SELECT count(*) as total FROM usager WHERE DATEDIFF(NOW(), usager.Date_Naissance)/365 > 50 AND usager.Civilite="Mr"';
				$resqueryH = $link->prepare($requeteH);
				$resqueryH -> execute();
				$resH = $resqueryH->fetch();

				//requête de selection des usagers femmes de plus de 50 ans
				$requeteF = 'SELECT count(*) as total FROM usager WHERE DATEDIFF(NOW(), usager.Date_Naissance)/365 > 50 AND usager.Civilite="Md"';
				$resqueryF = $link->prepare($requeteF);
				$resqueryF -> execute();
				$resF = $resqueryF->fetch();

				echo "<tr><td> Plus de 50 ans </td><td>".$resH['total']."</td><td>".$resF['total']."</td></tr>";
			?>
			</tbody>
		</table>
	<br>
</body>
</html>