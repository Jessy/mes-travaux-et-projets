<!DOCTYPE html>
<html>
<head>
	<link href="style.css" rel="stylesheet" type="text/css"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Index</title>
</head>
<body>
	<?php
		session_start();
		//connexion à la base de données
        require('connexionBaseDonnees.php');
        $link = connexionBD();

        $resquery = 'SELECT * FROM connexion';
        $recipesStatement = $link->prepare($resquery);
		$recipesStatement->execute();
		$recipes = $recipesStatement->fetch();

		$_SESSION['username'] = $recipes['Username'];
		$_SESSION['password'] = $recipes['Password'];

		$username=$_SESSION['username'];
		$password=$_SESSION['password'];
		$log=$_GET['LOG'] ?? ' ';
		$mdp=$_GET['MDP'] ?? ' ';
		if($log==$username && $mdp==$password) {
			$_SESSION['logged in']=True;
			include('menuglobal.php');
			echo '<h1>Bienvenue sur la page d\'accueil du cabinet medical</h1>';
		} else {
			$_SESSION['logged in']=NULL;
			echo "<br> <strong>Utilisateur non connecté</strong> <br>";
			include('authentification.php');
		}
		
	?>
</body>
</html>