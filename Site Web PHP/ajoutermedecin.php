<!DOCTYPE HTML>
<?php
	include('verifauthentification.php');
	verifConnexion();
	include('menuglobal.php');
?>
<html>
<head>
	<link rel="stylesheet" href="style.css">
	<title>Ajout Medecin</title>
</head>
<body>
	<h1>Ajouter un Médecin</h1>
	<form action="ajoutermedecin.php" method="post">
		<fieldset>
			<legend><strong>Saisie des informations</strong></legend>
			<label for="Civilite">Civilité (Mr/Md) : </label><input type="text" name="Civilite"><br>
			<label for="Nom">Nom : </label><input type="text" name="Nom"><br>
			<label for="Prenom">Prenom : </label><input type="text" name="Prenom"><br>
			<input type="submit" name="operation" value="Valider">
			<input type="reset" name="operation" value="Effacer">
		</fieldset>
	</form>
</body>
</html>

<?php
	if(!empty($_POST['Civilite']) && !empty($_POST['Nom']) && !empty($_POST['Prenom'])) {

		$Civ=$_POST['Civilite'];
		$Nom=$_POST['Nom'];
		$Pre=$_POST['Prenom'];

		//connexion à la base de données
				require('connexionBaseDonnees.php');
				$link = connexionBD();

		//requête d'ajout des informations du medecin
		$requete = 'INSERT INTO medecin (Civilite, Nom, Prenom) VALUES("'.$Civ.'","'.$Nom.'","'.$Pre.'")';
		$resquery = $link->prepare($requete);
		$resquery -> execute();
		echo "<h3>Medecin crée avec succés</h3>";
		return $resquery;
	}
?>