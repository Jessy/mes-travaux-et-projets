<!DOCTYPE html>
<?php
include('verifauthentification.php');
verifConnexion();
include('menuglobal.php');
//connexion à la base de données
                require('connexionBaseDonnees.php');
                $link = connexionBD();

// Si tout va bien, on peut continuer
// On récupère tout le contenu de la table
$sqlQuery = 'SELECT * FROM usager';
$recipesStatement = $link->prepare($sqlQuery);
$recipesStatement->execute();
$recipes = $recipesStatement->fetchAll();
?>

<html>
<head>
    <link rel="stylesheet" href="style.css">
	<meta charset="utf-8">
	<title></title>
</head>
<style>
table, th, td {
  border:1px solid black;
}
</style>
<body>
    <h1>Liste des Usagers</h1>
	 <table>
      <tr>
        
        <th>Civilité</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Ville</th>
        <th>Code Postal</th>
        <th>Adresse</th>
        <th>Date de naissance</th>
        <th>Lieu de naissance</th>
        <th>Numéro de sécurité sociale</th>
        
        
      </tr>
      <tr>
        <td>

          <?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Civilite']; ?></p>
<?php
}
?></td>
        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Nom']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Prenom']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Ville']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['CP']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Adresse']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Date_Naissance']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Lieu_Naissance']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Numero_Secu']; ?></p>
<?php
}
?></td>



        <td><?php
foreach ($recipes as $recipe) {
  $id=$recipe['Id_Usager'];
?>
    <p><a href="modificationusager.php?id=<?php echo urlencode($id);?>">Modification</a></p>
<?php

}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
    $id=$recipe['Id_Usager'];
?>
    <p><a href="suppruser.php?id=<?php echo urlencode($id);?>" target="_blank" > Supprimer</a></p>
<?php
}
?></td>





</body>
</html>