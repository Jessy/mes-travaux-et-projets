<html>
<head>
	<link rel="stylesheet" href="style.css">
	<title>AffichageHeuresMedecins</title>
	<style>
	table, th, td {
 	border:1px solid black;
	}
	</style>
</head>
<body>
	<?php
		include('verifauthentification.php');
		verifConnexion();
		include('menuglobal.php');
	?>
	<h1>Durée totale des consultations</h1>
		<table>
			<thead>
				<tr>
					<th>Médecin</th>
					<th>Total Consultation</th>
				</tr>
			</thead>
			<tbody>
			<?php
				//connexion à la base de données
				require('connexionBaseDonnees.php');
				$link = connexionBD();

				//requête de selection des heures de chaque médecin
				$requete = 'SELECT medecin.Nom, TIME_FORMAT(sum(consultation.Duree),"%H:%i Heures") as DureeTotale FROM consultation, medecin WHERE medecin.Id_Medecin = consultation.Id_Medecin GROUP BY medecin.Nom';
				$resquery = $link->prepare($requete);
				$resquery -> execute();
				$rows = $resquery->fetch();

				foreach($rows as $row) {
					echo "<tr><td>".$row."</td></tr>";
				}
			?>
			</tbody>
		</table>
	<br>
</body>
</html>