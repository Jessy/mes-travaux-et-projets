<!DOCTYPE HTML>
<?php
	include('verifauthentification.php');
	verifConnexion();
	include('menuglobal.php');
?>
<html>
<head>
	<link rel="stylesheet" href="style.css">
	<title>AffichageMedecins</title>
	<style>
	table, th, td {
 	border:1px solid black;
	}
	</style>
</head>
<body>
	<h1>Liste des Médecins</h1>
		<table>
			<thead>
				<tr>
					<th>Civilité</th>
					<th>Nom</th>
					<th>Prenom</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
					<?php
						//connexion à la base de données
						require('connexionBaseDonnees.php');
						$link = connexionBD();

						//requête de selection des données de médecin
						$requete = 'SELECT * FROM medecin';
						$resquery = $link->prepare($requete);
						$resquery -> execute();
						$rows = $resquery->fetchAll();

						//affichage des médecins dans le tableau
						foreach ($rows as $row) {
							$idMed = $row['Id_Medecin'];
							echo "<tr><td>".$row['Civilite']."</td><td>".$row['Nom']."</td><td>".$row['Prenom']."</td><td> 
							<a href='modifiermedecin.php?id=$idMed'>Modifier</a> <a href='supprimermedecin.php?id=$idMed'>Supprimer</a>"."</td></tr>";
						}
					?>
			</tbody>
		</table>
	<br>
</body>
</html>