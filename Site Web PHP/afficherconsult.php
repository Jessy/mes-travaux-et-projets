<!DOCTYPE html>
<?php
include('verifauthentification.php');
verifConnexion();
include('menuglobal.php');
//connexion à la base de données
                require('connexionBaseDonnees.php');
                $link = connexionBD();

// Si tout va bien, on peut continuer
// On récupère tout le contenu de la table
$sqlQuery = 'SELECT * FROM consultation order by Date_RDV desc, Heure_RDV desc';
$recipesStatement = $link->prepare($sqlQuery);
$recipesStatement->execute();
$recipes = $recipesStatement->fetchAll();

$sqlQuery = 'SELECT * FROM medecin,consultation where medecin.Id_Medecin=consultation.Id_Medecin ';
$recipesStatement = $link->prepare($sqlQuery);
$recipesStatement->execute();
$medecins = $recipesStatement->fetchAll();

$sqlQuery = 'SELECT * FROM usager,consultation where usager.Id_Usager=consultation.Id_Usager';
$recipesStatement = $link->prepare($sqlQuery);
$recipesStatement->execute();
$usagers = $recipesStatement->fetchAll();
?>

<html>
<head>
    <link rel="stylesheet" href="style.css">
	<meta charset="utf-8">
	<title></title>
</head>
<style>
table, th, td {
    border: 1px solid black;
    border-spacing: 0;

}
</style>
<body>
    <h1>Liste des Consultations</h1>
	 <table>
      <tr>
        
        <th>Date du rendez-vous</th>
        <th>Heure du rendez-vous</th>
        <th>Durée</th>
        <th>Patient</th>
        <th>Médecin</th>
      </tr>
      <tr>
        <td>

          <?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Date_RDV']; ?></p>
<?php
}
?></td>
        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Heure_RDV']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
?>
    <p><?php echo $recipe['Duree']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($medecins as $medecin) {
?>
    <p><?php echo $medecin['Civilite']; ?></p>
    <p><?php echo $medecin['Nom']; ?></p>
    <p><?php echo $medecin['Prenom']; ?></p>
<?php
}
?></td>

        <td><?php
foreach ($usagers as $usager) {
?>
    <p><?php echo $usager['Civilite']; ?></p>
    <p><?php echo $usager['Nom']; ?></p>
    <p><?php echo $usager['Prenom']; ?></p>
<?php
}
?></td>

<td><?php
foreach ($recipes as $recipe) {
  $id=$recipe['Id_Consultation'];
?>
    <p><a href="modificationconsult.php?id=<?php echo urlencode($id);?>">Modification</a></p>
<?php

}
?></td>

        <td><?php
foreach ($recipes as $recipe) {
    $id=$recipe['Id_Consultation'];
?>
    <p><a href="supprconsult.php?id=<?php echo urlencode($id);?>" target="_blank" > Supprimer</a></p>
<?php
}
?></td>
</tr>
</table>




</body>
</html>