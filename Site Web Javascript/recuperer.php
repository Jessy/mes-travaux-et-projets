<?php
	//connexion à la base de données
	$link = new PDO('mysql:host=localhost;dbname=mysql','root','root');

	//ouverture du fichier messages.html
	$myfile = fopen("messages.html", "w");

	//requete de recupération des 10 derniers messages
	$requete = 'SELECT * FROM( SELECT * FROM chat ORDER BY Id_Chat DESC LIMIT 10)Var1 ORDER BY Id_Chat ASC';
	$resquery = $link->prepare($requete);
	$resquery -> execute();
	$rows = $resquery->fetchAll();

	foreach ($rows as $row) {
		//Ajout du nom de l'auteur
		$txt = "<p><span style='font-weight:bold'>".$row["Auteur"];
		fwrite($myfile, $txt);

		//Ajout de la date
		$time = $row["Date_Heure"];
		$txt = "</span> le <span style='font-style:italic'>".date("d/m/Y",strtotime($time))."</span> à ".date("H:i:s",strtotime($time));
		fwrite($myfile, $txt);

		//Ajout du message
		$msg = $row["Contenu"];
		$txt = " : <span style='color:black;font-family:Verdana'>".$msg."</span>";
		fwrite($myfile, $txt);

		//retour à la ligne
		$txt = "</p> \n";
		fwrite($myfile, $txt);
	};
	fclose($myfile);
?>