<!DOCTYPE HTML>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Messagerie</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
	<!--on bloque l'envoie du formulaire pour ne pas rafraichir la page-->
	<form onsubmit="return false;">
		<div class="top"> 
			 <i class="fa fa-envelope icon"></i><input type="Text" name="Auteur" class="Author" id="Author" onkeydown="return event.key != 'Enter'">
		</div>

		<!--c'est ici que les messages seront affichés-->
		<div id="msg" class="messages">
		</div>

		<div class="bottom" align="center">
			<input id="Content" class="Content" type="Text" name="Contenu" placeholder="Ecrivez votre message" maxlength="100"><button onclick="envoie()">Envoyer</button>
		</div>
	</form>
</body>
</html>
<script type="text/javascript">

	//la fonction fait appel au fichier recuperer.php et charge les messages
	//stockés dans le fichier messages.html dans la balise div#msg
	function recupere() {
		$.get("recuperer.php", function(data) {
			$("#msg").load("messages.html");
		});
	}

	//une intervalle de 2secondes fait appel à la fonction recupere()
	setInterval("recupere()", 2000);
	
	//la fonction envoie() verifie que la messagerie possède un nom d'Auteur ainsi qu'un message avant l'envoi. 
	//Elle est appeler soit en appuyant sur le bouton "Envoyer" soit en appuyant sur la touche Entrer
	//Cependant, comme mon application possède un formulaire, au lieu d'Activer l'envoi lors de l'appui
	//avec la fonction JQuery, j'ai bloqué l'envoi du formulaire sur la balise input de l'Auteur
	//j'aurais pu ne pas utiliser un formulaire mais lorsque j'ai essayé de l'enlever, mon style css ne
	//fonctionnait plus correctement...
	function envoie() {
		if(document.getElementById("Author").value=="") {
			window.alert("Veuillez indiquer un auteur");
		} else {
			if(document.getElementById("Content").value=="") {
				window.alert("Message vide!");
			} else {
				var Aut = $("#Author").val();
				var Cont = $("#Content").val();
				$.get("enregistrer.php", {Auteur: Aut, Contenu: Cont},
					function() {
						$("#Content").val(""); //on vide la zone de saisie après envoi
					});
			}
		}	
	}
</script>